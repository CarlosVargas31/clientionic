import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Warehouse } from '../models/warehouse';

@Injectable({
  providedIn: 'root'
})
export class WarehouseService {

  readonly url = environment.backend + 'ServerStoreREST-ProjectServerStoreREST-context-root/resources/serviceREST/';

  constructor(private httpClient: HttpClient) { }

  getWarehouses(): Observable<Warehouse[]> {
    return this.httpClient.get<Warehouse[]>(this.url + 'getAllWarehouses');
  }

  findWarehouse(id: number): Observable<Warehouse> {
    const param = new HttpParams().set('id', id.toString());
    return this.httpClient.get<Warehouse>(this.url + 'findWarehouse', { params: param });
  }

}

import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Warehouse } from '../models/warehouse';

@Injectable({
  providedIn: 'root'
})
export class WarehouseFirestoreService {

  constructor(private angularFirestore: AngularFirestore) { }

  getWarehousesList(): AngularFirestoreCollection<Warehouse> {
    return this.angularFirestore.collection(`warehouses`);
  }

}

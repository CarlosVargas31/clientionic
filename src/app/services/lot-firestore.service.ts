import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Lot } from '../models/lot';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class LotFirestoreService {

  constructor(private angularFirestore: AngularFirestore) { }

  getLotList(): AngularFirestoreCollection<Lot> {
    return this.angularFirestore.collection(`lots`);
  }

  findLot(id: number): AngularFirestoreDocument<Lot> {
    return this.angularFirestore.collection(`lots`).doc(id.toString());
  }

  findLotsByProduct(product: Product): AngularFirestoreCollection<Lot> {
    return this.angularFirestore.collection(`lots`, ref => ref.where('product', '==', product));
  }

  findLotsByStock(stock: number): AngularFirestoreCollection<Lot> {
    return this.angularFirestore.collection(`lots`, ref => ref.where('stock', '==', stock));
  }

  saveLot(lot: Lot) {
    return this.angularFirestore.collection(`lots`).doc(lot.id.toString()).set(lot);
  }

  updateLot(id: number, lot: Lot) {
    return this.angularFirestore.collection(`lots`).doc(id.toString()).set(lot);
  }

  deleteLot(id: number) {
    return this.angularFirestore.collection(`lots`).doc(id.toString()).delete();
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Product } from '../models/product';
import { Lot } from '../models/lot';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  readonly url = environment.backend + 'ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Products/';

  constructor(private httpClient: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.url + 'getAllProducts');
  }

  getLotsProduct(code: number): Observable<Lot[]> {
    const param = new HttpParams().set('code', code.toString());
    return this.httpClient.get<Lot[]>(this.url + 'findLotsProduct', { params: param });
  }

  getProductsByName(name: string): Observable<Product[]> {
    const param = new HttpParams().set('name', name);
    return this.httpClient.get<Product[]>(this.url + 'getAllByNameLikeProduct', { params: param });
  }

  findProduct(code: number): Observable<Product> {
    const param = new HttpParams().set('code', code.toString());
    return this.httpClient.get<Product>(this.url + 'findProduct', { params: param });
  }

  saveProduct(product: Product): Observable<Product> {
    const options = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    return this.httpClient.post<Product>(this.url + 'saveProduct', product, options);
  }

  updateProduct(code: number, product: Product): Observable<Product> {
    const param = new HttpParams().set('code', code.toString());
    return this.httpClient.put<Product>(this.url + 'updateProduct', product, { params: param });
  }

  deleteProduct(code: number): Observable<Product> {
    const param = new HttpParams().set('code', code.toString());
    return this.httpClient.delete<Product>(this.url + 'removeProduct', { params: param });
  }

}

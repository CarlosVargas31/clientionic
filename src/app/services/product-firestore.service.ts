import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductFirestoreService {

  constructor(private angularFirestore: AngularFirestore) { }

  getProductList(): AngularFirestoreCollection<Product> {
    return this.angularFirestore.collection(`products`);
  }

  findProduct(code: number): AngularFirestoreDocument<Product> {
    return this.angularFirestore.collection(`products`).doc(code.toString());
  }

}

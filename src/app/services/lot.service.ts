import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Lot } from '../models/lot';

@Injectable({
  providedIn: 'root'
})
export class LotService {

  readonly url = environment.backend + 'ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/';

  constructor(private httpClient: HttpClient) { }

  getLots(): Observable<Lot[]> {
    return this.httpClient.get<Lot[]>(this.url + 'getAll');
  }

  getLotsByStock(stock: number): Observable<Lot[]> {
    const param = new HttpParams().set('stock', stock.toString());
    return this.httpClient.get<Lot[]>(this.url + 'findByStock', { params: param });
  }

  getLotsByProduct(code: number): Observable<Lot[]> {
    const param = new HttpParams().set('code', code.toString());
    return this.httpClient.get<Lot[]>(this.url + 'findByProduct', { params: param });
  }

  findLot(id: number): Observable<Lot> {
    const param = new HttpParams().set('id', id.toString());
    return this.httpClient.get<Lot>(this.url + 'find', { params: param });
  }

  saveLot(lot: Lot, code: number, id: number): Observable<Lot> {
    let param = new HttpParams();
    param = param.append('code', code.toString());
    param = param.append('id', id.toString());
    return this.httpClient.post<Lot>(this.url + 'save', lot, { params: param });
  }

  updateLot(id: number, lot: Lot): Observable<Lot> {
    const param = new HttpParams().set('id', id.toString());
    return this.httpClient.put<Lot>(this.url + 'update', lot, { params: param });
  }

  deleteLot(id: number): Observable<Lot> {
    const param = new HttpParams().set('id', id.toString());
    return this.httpClient.delete<Lot>(this.url + 'remove', { params: param });
  }

}

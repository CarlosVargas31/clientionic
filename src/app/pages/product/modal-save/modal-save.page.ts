import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ModalController } from '@ionic/angular';

import { ProductService } from 'src/app/services/product.service';

import { Product } from 'src/app/models/product';


@Component({
  selector: 'app-modal-save',
  templateUrl: './modal-save.page.html',
  styleUrls: ['./modal-save.page.scss'],
})
export class ModalSavePage implements OnInit {

  product: Product = {
    code: null,
    name: '',
    price: null
  };

  constructor(private modalController: ModalController, private productService: ProductService) { }

  ngOnInit() { }

  close() {
    this.modalController.dismiss();
  }

  onSave(form: NgForm) {
    console.log(form.value);
    this.productService.saveProduct(form.value)
    .subscribe(data => {
      console.log(data);
    });
    this.close();
  }

}

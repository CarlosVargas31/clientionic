import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ProductService } from 'src/app/services/product.service';

import { Product } from '../../../models/product';
import { Lot } from '../../../models/lot';

@Component({
  selector: 'app-modal-find',
  templateUrl: './modal-find.page.html',
  styleUrls: ['./modal-find.page.scss'],
})
export class ModalFindPage implements OnInit {

  parameter = '';

  product: Product = {
    code: null,
    name: '',
    price: null
  };

  products: Product [] = [];

  lots: Lot [] = [];

  constructor(private modalController: ModalController, private productService: ProductService) { }

  ngOnInit() { }

  findProduct(code: number) {
    this.products = [];
    this.lots = [];

    this.productService.findProduct(code)
    .subscribe(data => {
      this.product = data;
    });
  }

  getLots(code: number) {
    this.products = [];
    this.product = {
      code: null,
      name: '',
      price: null
    };

    this.productService.getLotsProduct(code)
    .subscribe(data => {
      this.lots = data;
    });
  }

  getProductsByName(name: string) {
    this.lots = [];
    this.product = {
      code: null,
      name: '',
      price: null
    };

    this.productService.getProductsByName(name).subscribe(data => {
      this.products = data;
    });
  }

  close() {
    this.modalController.dismiss();
  }

}

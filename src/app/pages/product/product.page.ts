import { Component, OnInit } from '@angular/core';

import { ModalController, ActionSheetController } from '@ionic/angular';

import { Product } from 'src/app/models/product';

import { ProductService } from 'src/app/services/product.service';

import { ModalSavePage } from './modal-save/modal-save.page';
import { ModalUpdatePage } from './modal-update/modal-update.page';
import { ModalFindPage } from './modal-find/modal-find.page';
import { ProductFirestoreService } from '../../services/product-firestore.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  products: Product [] = [];

  listProducts: Observable<Product[]>;

  constructor(private productService: ProductService, private modalController: ModalController,
              private actionSheetController: ActionSheetController, private firestoreService: ProductFirestoreService) { }

  ngOnInit() {
    /*this.productService.getProducts()
    .subscribe(data => {
      this.products = data;
    });*/
    this.listProducts = this.firestoreService.getProductList().valueChanges();
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: ModalSavePage
    });

    await modal.present();
  }

  async openModalUpdate(product: Product) {
    const modal = await this.modalController.create({
      component: ModalUpdatePage,
      componentProps: {
        product
      }
    });

    await modal.present();
  }

  async openModalFind() {
    const modal = await this.modalController.create({
      component: ModalFindPage
    });

    await modal.present();
  }

  async presentActionSheet(code: number, product: Product) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log(code);
          this.productService.deleteProduct(code)
          .subscribe(data => {
            console.log(data);
          });
        }
      }, {
        text: 'Update',
        icon: 'bookmark-outline',
        handler: () => {
          console.log(product);
          this.openModalUpdate(product);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

}

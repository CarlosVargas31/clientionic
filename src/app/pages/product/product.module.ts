import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductPageRoutingModule } from './product-routing.module';
import { ModalSavePageModule } from './modal-save/modal-save.module';
import { ModalUpdatePageModule } from './modal-update/modal-update.module';
import { ModalFindPageModule } from './modal-find/modal-find.module';

import { ProductPage } from './product.page';
import { ModalSavePage } from './modal-save/modal-save.page';
import { ModalUpdatePage } from './modal-update/modal-update.page';
import { ModalFindPage } from './modal-find/modal-find.page';

import { ProductService } from '../../services/product.service';
import { ProductFirestoreService } from '../../services/product-firestore.service';

@NgModule({
  entryComponents: [
    ModalSavePage,
    ModalUpdatePage,
    ModalFindPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductPageRoutingModule,
    ModalSavePageModule,
    ModalUpdatePageModule,
    ModalFindPageModule
  ],
  providers: [ProductService, ProductFirestoreService],
  declarations: [ProductPage]
})
export class ProductPageModule {}

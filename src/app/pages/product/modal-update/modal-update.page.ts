import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ModalController } from '@ionic/angular';

import { ProductService } from 'src/app/services/product.service';

import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-modal-update',
  templateUrl: './modal-update.page.html',
  styleUrls: ['./modal-update.page.scss'],
})
export class ModalUpdatePage implements OnInit {

  @Input() product: Product;

  constructor(private modalController: ModalController, private productService: ProductService) { }

  ngOnInit() { }

  close() {
    this.modalController.dismiss();
  }

  onUpdate(form: NgForm) {
    console.log(form.value);
    this.productService.updateProduct(this.product.code, form.value)
    .subscribe(data => {
      console.log(data);
    });
    this.close();
  }

}

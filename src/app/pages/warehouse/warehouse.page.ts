import { Component, OnInit } from '@angular/core';

import { WarehouseService } from '../../services/warehouse.service';

import { Warehouse } from '../../models/warehouse';
import { Observable } from 'rxjs';
import { WarehouseFirestoreService } from '../../services/warehouse-firestore.service';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.page.html',
  styleUrls: ['./warehouse.page.scss'],
})
export class WarehousePage implements OnInit {

  warehouses: Warehouse [] = [];

  warehousesList: Observable<Warehouse[]>;

  constructor(private warehouseService: WarehouseService, private firestore: WarehouseFirestoreService) { }

  ngOnInit() {
    /*this.warehouseService.getWarehouses()
    .subscribe(data => {
      this.warehouses = data;
    });*/
    this.warehousesList = this.firestore.getWarehousesList().valueChanges();
  }

}

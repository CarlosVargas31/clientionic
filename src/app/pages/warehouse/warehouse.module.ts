import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WarehousePageRoutingModule } from './warehouse-routing.module';

import { WarehousePage } from './warehouse.page';

import { WarehouseService } from '../../services/warehouse.service';
import { WarehouseFirestoreService } from '../../services/warehouse-firestore.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WarehousePageRoutingModule
  ],
  providers: [
    WarehouseService,
    WarehouseFirestoreService
  ],
  declarations: [WarehousePage]
})
export class WarehousePageModule {}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ModalController } from '@ionic/angular';

import { Lot } from 'src/app/models/lot';
import { Product } from 'src/app/models/product';
import { Warehouse } from 'src/app/models/warehouse';

import { LotService } from 'src/app/services/lot.service';
import { ProductService } from 'src/app/services/product.service';
import { WarehouseService } from 'src/app/services/warehouse.service';
import { ProductFirestoreService } from '../../../services/product-firestore.service';
import { WarehouseFirestoreService } from '../../../services/warehouse-firestore.service';
import { Observable } from 'rxjs';
import { LotFirestoreService } from '../../../services/lot-firestore.service';

@Component({
  selector: 'app-modal-save-lot',
  templateUrl: './modal-save-lot.page.html',
  styleUrls: ['./modal-save-lot.page.scss'],
})
export class ModalSaveLotPage implements OnInit {

  lot: Lot = {
    id: null,
    warehouse: null,
    product: null,
    stock: null
  };

  codeProduct: number = null;

  idWarehouse: number = null;

  products: Product [] = [];

  warehouses: Warehouse [] = [];

  productsList: Observable<Product[]>;

  warehousesList: Observable<Warehouse[]>;

  constructor(private modalController: ModalController, private lotService: LotService,
              private productService: ProductService, private warehouseService: WarehouseService,
              private productFirestore: ProductFirestoreService, private warehouseFirestore: WarehouseFirestoreService,
              private lotFirestore: LotFirestoreService) { }

  ngOnInit() {
    /*this.productService.getProducts()
    .subscribe(data => {
      this.products = data;
    });
    this.warehouseService.getWarehouses()
    .subscribe(data => {
      this.warehouses = data;
    });*/
    this.productsList = this.productFirestore.getProductList().valueChanges();
    this.warehousesList = this.warehouseFirestore.getWarehousesList().valueChanges();
  }

  onSave(form: NgForm) {
    /*console.log(form.value);
    this.lotService.saveLot(form.value, this.codeProduct, this.idWarehouse)
    .subscribe(data => {
      console.log(data);
    });*/
    this.lotFirestore.saveLot(form.value);
    this.close();
  }

  close() {
    this.modalController.dismiss();
  }

}

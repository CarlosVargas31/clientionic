import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ModalController } from '@ionic/angular';

import { Lot } from '../../../models/lot';
import { Product } from '../../../models/product';
import { Warehouse } from '../../../models/warehouse';

import { ProductService } from 'src/app/services/product.service';
import { LotService } from '../../../services/lot.service';
import { WarehouseService } from '../../../services/warehouse.service';
import { ProductFirestoreService } from 'src/app/services/product-firestore.service';
import { WarehouseFirestoreService } from 'src/app/services/warehouse-firestore.service';
import { LotFirestoreService } from 'src/app/services/lot-firestore.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-modal-update-lot',
  templateUrl: './modal-update-lot.page.html',
  styleUrls: ['./modal-update-lot.page.scss'],
})
export class ModalUpdateLotPage implements OnInit {

  @Input() lot: Lot;

  products: Product [] = [];

  warehouses: Warehouse [] = [];

  productsList: Observable<Product[]>;

  warehousesList: Observable<Warehouse[]>;

  constructor(private modalController: ModalController, private lotService: LotService,
              private productService: ProductService, private warehouseService: WarehouseService,
              private productFirestore: ProductFirestoreService, private warehouseFirestore: WarehouseFirestoreService,
              private lotFirestore: LotFirestoreService) { }

  ngOnInit() {
    /*this.productService.getProducts()
    .subscribe(data => {
      this.products = data;
    });
    this.warehouseService.getWarehouses()
    .subscribe(data => {
      this.warehouses = data;
    });*/
    this.productsList = this.productFirestore.getProductList().valueChanges();
    this.warehousesList = this.warehouseFirestore.getWarehousesList().valueChanges();
  }

  onUpdate(form: NgForm) {
    console.log(form.value);
    console.log(this.lot.id);
    /*this.lotService.updateLot(this.lot.id, form.value)
    .subscribe(data => {
      console.log(data);
    });*/
    this.lotFirestore.updateLot(this.lot.id, form.value);
    this.close();
  }

  close() {
    this.modalController.dismiss();
  }

}

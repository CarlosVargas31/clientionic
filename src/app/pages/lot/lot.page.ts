import { Component, OnInit } from '@angular/core';

import { ModalController, ActionSheetController } from '@ionic/angular';

import { LotService } from '../../services/lot.service';

import { Lot } from './../../models/lot';

import { ModalFindLotPage } from './modal-find-lot/modal-find-lot.page';
import { ModalUpdateLotPage } from './modal-update-lot/modal-update-lot.page';
import { ModalSaveLotPage } from './modal-save-lot/modal-save-lot.page';
import { Observable } from 'rxjs';
import { LotFirestoreService } from '../../services/lot-firestore.service';

@Component({
  selector: 'app-lot',
  templateUrl: './lot.page.html',
  styleUrls: ['./lot.page.scss'],
})
export class LotPage implements OnInit {

  lots: Lot [] = [];

  lotsList: Observable<Lot[]>;

  constructor(private lotService: LotService, private modalController: ModalController,
              private actionSheetController: ActionSheetController, private lotFirestoreService: LotFirestoreService) { }

  ngOnInit() {
    /*this.lotService.getLots()
    .subscribe(data => {
      this.lots = data;
    });*/
    this.lotsList = this.lotFirestoreService.getLotList().valueChanges();
  }

  async openModalFind() {
    const modal = await this.modalController.create({
      component: ModalFindLotPage
    });

    await modal.present();
  }

  async openModalUpdate(lot: Lot) {
    const modal = await this.modalController.create({
      component: ModalUpdateLotPage,
      componentProps: {
        lot
      }
    });

    await modal.present();
  }

  async openModalSave() {
    const modal = await this.modalController.create({
      component: ModalSaveLotPage
    });

    await modal.present();
  }

  async presentActionSheet(id: number, lot: Lot) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log(id);
          /*this.lotService.deleteLot(id)
          .subscribe(data => {
            console.log(data);
          });*/
          this.lotFirestoreService.deleteLot(id);
        }
      }, {
        text: 'Update',
        icon: 'bookmark-outline',
        handler: () => {
          console.log(lot);
          this.openModalUpdate(lot);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

}

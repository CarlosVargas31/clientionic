import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LotPageRoutingModule } from './lot-routing.module';
import { ModalFindLotPageModule } from './modal-find-lot/modal-find-lot.module';
import { ModalUpdateLotPageModule } from './modal-update-lot/modal-update-lot.module';
import { ModalSaveLotPageModule } from './modal-save-lot/modal-save-lot.module';

import { LotPage } from './lot.page';

import { ModalFindLotPage } from './modal-find-lot/modal-find-lot.page';
import { ModalUpdateLotPage } from './modal-update-lot/modal-update-lot.page';
import { ModalSaveLotPage } from './modal-save-lot/modal-save-lot.page';

import { ProductService } from '../../services/product.service';
import { WarehouseService } from '../../services/warehouse.service';
import { LotService } from '../../services/lot.service';
import { LotFirestoreService } from '../../services/lot-firestore.service';

@NgModule({
  entryComponents: [
    ModalFindLotPage,
    ModalUpdateLotPage,
    ModalSaveLotPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LotPageRoutingModule,
    ModalFindLotPageModule,
    ModalUpdateLotPageModule,
    ModalSaveLotPageModule
  ],
  providers: [
    ProductService,
    WarehouseService,
    LotService,
    LotFirestoreService
  ],
  declarations: [LotPage]
})
export class LotPageModule {}

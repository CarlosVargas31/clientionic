import { Component, OnInit } from '@angular/core';

import { AlertController, ModalController } from '@ionic/angular';

import { Lot } from 'src/app/models/lot';
import { Product } from 'src/app/models/product';

import { LotService } from 'src/app/services/lot.service';
import { ProductService } from '../../../services/product.service';
import { ProductFirestoreService } from '../../../services/product-firestore.service';
import { LotFirestoreService } from '../../../services/lot-firestore.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-modal-find-lot',
  templateUrl: './modal-find-lot.page.html',
  styleUrls: ['./modal-find-lot.page.scss'],
})
export class ModalFindLotPage implements OnInit {

  parameter = '';

  lots: Lot [] = [];

  products: Product [] = [];

  codeProduct: number = null;

  lot: Lot = {
    id: null,
    warehouse: null,
    product: null,
    stock: null
  };

  product: Product = {
    code: null,
    name: '',
    price: null
  };

  productsList: Observable<Product[]>;

  lotFind: Observable<Lot>;

  productFind: Observable<Product>;

  constructor(private modalController: ModalController, private alertCtrl: AlertController,
              private productService: ProductService, private lotService: LotService,
              private productFirestore: ProductFirestoreService, private lotFirestore: LotFirestoreService) { }

  ngOnInit() {
    /*this.productService.getProducts()
    .subscribe(data => {
      this.products = data;
    });*/
    this.productsList = this.productFirestore.getProductList().valueChanges();
  }

  findLot(id: number) {
    this.lots = [];

    /*this.lotService.findLot(id)
    .subscribe(data => {
      this.lot = data;
    }, async err => {
      console.log(err);
      const alert = await this.alertCtrl.create({
        cssClass: 'my-custom-class',
        header: 'Alert',
        message: 'This lot does not exist',
        buttons: ['OK']
      });
      await alert.present();
    });*/
    this.lotFind = this.lotFirestore.findLot(id).valueChanges();
    this.lotFind.subscribe(data => {
      this.lot = data;

      if (data == null) {
        this.presentAlert();
        console.log('Error');
      }
    });

    this.parameter = '';
  }

  getLotsByProduct() {
    this.lot = {
      id: null,
      warehouse: null,
      product: null,
      stock: null
    };
    /*this.lotService.getLotsByProduct(this.codeProduct)
    .subscribe(data => {
      this.lots = data;
    }, async err => {
      console.log(err);
      const alert = await this.alertCtrl.create({
        cssClass: 'my-custom-class',
        header: 'Alert',
        message: 'This product does not have lots',
        buttons: ['OK']
      });
      await alert.present();
    });*/

    this.productFind = this.productFirestore.findProduct(this.codeProduct).valueChanges();
    this.productFind.subscribe(doc => {
      this.lotFirestore.findLotsByProduct(doc).valueChanges()
      .subscribe(data => {
        this.lots = data;

        if (data.length === 0) {
          this.presentAlert();
          console.log('Error');
        }
      });
    });
  }

  getLotsByStock(stock: number) {
    this.lot = {
      id: null,
      warehouse: null,
      product: null,
      stock: null
    };

    /*this.lotService.getLotsByStock(stock)
    .subscribe(data => {
      this.lots = data;
    });*/

    this.lotFirestore.findLotsByStock(stock).valueChanges()
    .subscribe(data => {
      this.lots = data;

      if (data.length === 0) {
        this.presentAlert();
        console.log('Error');
      }
    });

    this.parameter = '';
  }

  close() {
    this.modalController.dismiss();
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      subHeader: 'Do not found this element',
      buttons: ['Dismiss']
    });
    alert.present();
  }

}

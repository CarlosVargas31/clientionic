import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AlertController, IonicModule } from '@ionic/angular';

import { ModalFindLotPage } from './modal-find-lot.page';

import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule
  ],
  providers: [
    AlertController
  ],
  declarations: [ModalFindLotPage]
})
export class ModalFindLotPageModule {}

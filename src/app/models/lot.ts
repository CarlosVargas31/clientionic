import { Product } from './product';
import { Warehouse } from './warehouse';

export interface Lot {

    id: number;
    warehouse: Warehouse;
    product: Product;
    stock: number;

}

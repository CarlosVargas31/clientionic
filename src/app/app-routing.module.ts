import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule) },
  { path: 'product', loadChildren: () => import('./pages/product/product.module').then( m => m.ProductPageModule) },
  { path: 'lot', loadChildren: () => import('./pages/lot/lot.module').then( m => m.LotPageModule) },
  { path: 'warehouse', loadChildren: () => import('./pages/warehouse/warehouse.module').then( m => m.WarehousePageModule) },
  { path: 'contact', loadChildren: () => import('./pages/contact/contact.module').then( m => m.ContactPageModule) }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

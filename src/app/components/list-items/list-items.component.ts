import { Component, OnInit, Input } from '@angular/core';

import { ProductService } from 'src/app/services/product.service';

import { Product } from '../../models/product';
import { Lot } from '../../models/lot';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss'],
})
export class ListItemsComponent implements OnInit {

  @Input() productItems: Product [] = [];

  @Input() lotItems: Lot [] = [];

  constructor() { }

  ngOnInit() {}

}

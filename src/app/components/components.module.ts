import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { ListItemsComponent } from './list-items/list-items.component';


@NgModule({
  declarations: [
    ListItemsComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    ListItemsComponent
  ]
})
export class ComponentsModule { }

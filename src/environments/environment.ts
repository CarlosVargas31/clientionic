// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backend: 'http://7f9a1cdaffee.ngrok.io/',
  firebaseConfig: {
    apiKey: 'AIzaSyCx1nyYiwwHccoEFWEONiLgfqMKBlUL-I8',
    authDomain: 'proyectosoa2020tiendacv.firebaseapp.com',
    databaseURL: 'https://proyectosoa2020tiendacv.firebaseio.com',
    projectId: 'proyectosoa2020tiendacv',
    storageBucket: 'proyectosoa2020tiendacv.appspot.com',
    messagingSenderId: '498576668827',
    appId: '1:498576668827:web:415068e308f765bd5e6e3e',
    measurementId: 'G-WW72LD5NX5'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
